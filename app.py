from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text  # Import text

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///populationestimation.db'
db = SQLAlchemy(app)

app.app_context().push()

class PopulationEstimation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    initial_population = db.Column(db.Integer)
    additional_population = db.Column(db.Integer)
    number_of_periods = db.Column(db.Integer)
    population_over_time = db.Column(db.Integer)

    def __init__(self, initial_population, additional_population, number_of_periods, population_over_time):
        self.initial_population = initial_population
        self.additional_population = additional_population
        self.number_of_periods = number_of_periods
        self.population_over_time = population_over_time

db.create_all()


@app.route('/population-estimation', methods=['POST'])
def population_estimation():
    def f(n):
        if n <= 1:
            return n
        return f(n - 1) + f(n - 2)
    
    data = request.get_json()
    initial_population = data.get('initial_population', 100)
    additional_population = data.get('additional_population', 50)
    number_of_periods = data.get('number_of_periods', 12)

    population_over_time = []
    for period in range(number_of_periods):
        next_population = (initial_population + additional_population) * f(period)
        population_over_time.append(next_population)

    

    with db.engine.connect() as connection:
        connection.execute("INSERT INTO population_estimation (initial_population, additional_population, number_of_periods, population_over_time) VALUES (" + str(initial_population) + ","+ str(additional_population) +","+str(number_of_periods)+","+str(population_over_time[-1]) + ")")

    return jsonify({"population_over_time": population_over_time})

if __name__ == '__main__':
    app.run(debug=True)
